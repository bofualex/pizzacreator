

import Foundation
import CoreData


@objc(PizzaModel)
class PizzaModel: NSManagedObject {
    
    @NSManaged var date: Date
    @NSManaged var ingredients: [String]
    @NSManaged var quantity: [Int]
}
