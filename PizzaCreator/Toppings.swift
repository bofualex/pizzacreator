


import Foundation
import UIKit

class Toppings: UIViewController {
    
    @IBOutlet var backIcon: UIButton!
    var identifiers: ([String], [Int]) = ([], [])
    
    private var toppingArray: [UIButton] = []
    private var toppingLabels: [UILabel] = []
    private var toppingConstraintsTop: [NSLayoutConstraint] = []
    private var toppingConstraintsRight: [NSLayoutConstraint] = []
    private var toppingLabelsConstraintsTop: [NSLayoutConstraint] = []
    private var toppingLabelsConstraintsRight: [NSLayoutConstraint] = []
    
    private var meatTuple: ([UIButton], [UIStepper], [UILabel]) = ([], [], [])
    private var meatTupleConstraints: ([NSLayoutConstraint], [NSLayoutConstraint], [NSLayoutConstraint]) = ([], [], [])
    
    private var veggTuple: ([UIButton], [UIStepper], [UILabel]) = ([], [], [])
    private var veggTupleConstraints: ([NSLayoutConstraint], [NSLayoutConstraint], [NSLayoutConstraint]) = ([], [], [])
    
    private var exoticTuple: ([UIButton], [UIStepper], [UILabel]) = ([], [], [])
    private var exoticTupleConstraints: ([NSLayoutConstraint], [NSLayoutConstraint], [NSLayoutConstraint]) = ([], [], [])
    
    private var cheeseTuple: ([UIButton], [UIStepper], [UILabel]) = ([], [], [])
    private var cheeseTupleConstraints: ([NSLayoutConstraint], [NSLayoutConstraint], [NSLayoutConstraint]) = ([], [], [])
    
    //MARK: - instance methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        initButtons()
        resetConstraints()
        animateButtonsRight()
    }
    
    private func initButtons() {
        toppingArray = Menus.generateButtons(view, buttonType: Buttons.toppingButton, backColor: .yellow)
        toppingConstraintsTop = Constraints.generateConstraintsTop(view, button1: toppingArray, constant: 40)
        toppingConstraintsRight = Constraints.generateConstraintsRight(view, button1: toppingArray, constant: -10)
        toppingLabels = Menus.createLabels(view, names: Strings.toppingString, backColor: .yellow)
        toppingLabelsConstraintsTop = Constraints.generateConstraintsTop(view, button1: toppingLabels, constant: 40)
        toppingLabelsConstraintsRight = Constraints.generateConstraintsRight(view, button1: toppingLabels, constant: -50)
        
        meatTuple = Menus.generateIngredientsTuple(view, buttonType: Buttons.meatButton, names: Others.sortAlph(Strings.meatString), backColor: .yellow, constant: -75)
        meatTupleConstraints = Constraints.generateConstraintsTopTuple(view, button: meatTuple.0, labels: meatTuple.2, steppers: meatTuple.1, constant: 40)
        
        exoticTuple = Menus.generateIngredientsTuple(view, buttonType: Buttons.exoticButton, names: Others.sortAlph(Strings.exoticString), backColor: .yellow, constant: -75)
        exoticTupleConstraints = Constraints.generateConstraintsTopTuple(view, button: exoticTuple.0, labels: exoticTuple.2, steppers: exoticTuple.1, constant: 40)
        
        veggTuple = Menus.generateIngredientsTuple(view, buttonType: Buttons.veggButton, names: Others.sortAlph(Strings.veggString), backColor: .yellow, constant: -75)
        veggTupleConstraints = Constraints.generateConstraintsTopTuple(view, button: veggTuple.0, labels: veggTuple.2, steppers: veggTuple.1, constant: 40)
        
        cheeseTuple = Menus.generateIngredientsTuple(view, buttonType: Buttons.cheeseButton, names: Others.sortAlph(Strings.cheeseString), backColor: .yellow, constant: -75)
        cheeseTupleConstraints = Constraints.generateConstraintsTopTuple(view, button: cheeseTuple.0, labels: cheeseTuple.2, steppers: cheeseTuple.1, constant: 40)
        
        backIcon?.addTarget(self, action: #selector(animateButtonsRight), for: .touchUpInside)
        
        toppingArray[0].addTarget(self, action: #selector(animateButtonsRightCheese), for: .touchUpInside)
        toppingArray[1].addTarget(self, action: #selector(animateButtonsRightExotic), for: .touchUpInside)
        toppingArray[2].addTarget(self, action: #selector(animateButtonsRightMeat), for: .touchUpInside)
        toppingArray[3].addTarget(self, action: #selector(animateButtonsRightVeggies), for: .touchUpInside)
    }
    
    private func resetConstraints() {
        Constraints.resetConstraints(toppingConstraintsTop)
        Constraints.resetConstraints(toppingLabelsConstraintsTop)
        Menus.setAlpha(toppingArray, alpha: 0)
        Menus.setAlphaLabel(toppingLabels, alpha: 0)
        
        Constraints.resetConstraintsTuple(meatTupleConstraints.0, constraint1: meatTupleConstraints.1, constraint2: meatTupleConstraints.2)
        Menus.setAlphaTuple(meatTuple.0, labels: meatTuple.2, steppers: meatTuple.1, alpha: 0)
        
        Constraints.resetConstraintsTuple(cheeseTupleConstraints.0, constraint1: cheeseTupleConstraints.1, constraint2: cheeseTupleConstraints.2)
        Menus.setAlphaTuple(cheeseTuple.0, labels: cheeseTuple.2, steppers: cheeseTuple.1, alpha: 0)
        
        Constraints.resetConstraintsTuple(veggTupleConstraints.0, constraint1: veggTupleConstraints.1, constraint2: veggTupleConstraints.2)
        Menus.setAlphaTuple(veggTuple.0, labels: veggTuple.2, steppers: veggTuple.1, alpha: 0)
        
        Constraints.resetConstraintsTuple(exoticTupleConstraints.0, constraint1: exoticTupleConstraints.1, constraint2: exoticTupleConstraints.2)
        Menus.setAlphaTuple(exoticTuple.0, labels: exoticTuple.2, steppers: exoticTuple.1, alpha: 0)
        
    }
    
    //MARK: - menu methods
    
    @objc private func animateButtonsRight() {
        resetConstraints()
        Constraints.setConstraints(toppingConstraintsTop, radius: 0, x: 0, y: 45)
        Constraints.setConstraints(toppingLabelsConstraintsTop, radius: 0, x: -5, y: 45)
        backIcon.alpha = 0
        UIView .animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            Menus.setEnabled(self.toppingArray, enabled: true)
            Menus.setAlpha(self.toppingArray, alpha: 1)
            Menus.setAlphaLabel(self.toppingLabels, alpha: 1)
        }
    }
    
    @objc private func animateButtonsRightMeat() {
        Constraints.setConstraintsTuple(meatTupleConstraints.0, constraint2: meatTupleConstraints.1, constraint3: meatTupleConstraints.2, x: -40, y: 45)
        
        for i in 0..<meatTuple.1.count {
            meatTuple.1[i].addTarget(self, action: #selector(stepperValueChanged), for: .touchUpInside)
        }
        
        UIView .animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            Menus.setAlphaTuple(self.meatTuple.0, labels: self.meatTuple.2, steppers: self.meatTuple.1, alpha: 1)
            
            Menus.setAlpha(self.toppingArray, alpha: 0)
            Menus.setAlphaLabel(self.toppingLabels, alpha: 0)
            
            self.toppingArray[2].alpha = 1
            self.toppingArray[2].isEnabled = false
            
            self.backIcon.alpha = 1
        }
    }
    
    @objc private func animateButtonsRightVeggies() {
        Constraints.setConstraintsTuple(veggTupleConstraints.0, constraint2: veggTupleConstraints.1, constraint3: veggTupleConstraints.2, x: -130, y: 45)
        
        for i in 0..<veggTuple.1.count {
            veggTuple.1[i].addTarget(self, action: #selector(stepperValueChanged), for: .touchUpInside)
        }
        
        UIView .animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            Menus.setAlphaTuple(self.veggTuple.0, labels: self.veggTuple.2, steppers: self.veggTuple.1, alpha: 1)
            
            Menus.setAlpha(self.toppingArray, alpha: 0)
            Menus.setAlphaLabel(self.toppingLabels, alpha: 0)
            
            self.toppingArray[3].alpha = 1
            self.toppingArray[3].isEnabled = false
            
            self.backIcon.alpha = 1
        }
    }
    
    @objc private func animateButtonsRightCheese() {
        Constraints.setConstraintsTuple(cheeseTupleConstraints.0, constraint2: cheeseTupleConstraints.1, constraint3: cheeseTupleConstraints.2, x: -85, y: 45)
        
        for i in 0..<cheeseTuple.1.count {
            cheeseTuple.1[i].addTarget(self, action: #selector(stepperValueChanged), for: .touchUpInside)
        }
        
        UIView .animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            Menus.setAlphaTuple(self.cheeseTuple.0, labels: self.cheeseTuple.2, steppers: self.cheeseTuple.1, alpha: 1)
            
            Menus.setAlpha(self.toppingArray, alpha: 0)
            Menus.setAlphaLabel(self.toppingLabels, alpha: 0)
            
            self.toppingArray[0].alpha = 1
            self.toppingArray[0].isEnabled = false
            
            self.backIcon.alpha = 1
        }
    }
    
    @objc private func animateButtonsRightExotic() {
        Constraints.setConstraintsTuple(exoticTupleConstraints.0, constraint2: exoticTupleConstraints.1, constraint3: exoticTupleConstraints.2, x: -40, y: 45)
        
        for i in 0..<exoticTuple.1.count {
            exoticTuple.1[i].addTarget(self, action: #selector(stepperValueChanged), for: .touchUpInside)
        }
        
        UIView .animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            Menus.setAlphaTuple(self.exoticTuple.0, labels: self.exoticTuple.2, steppers: self.exoticTuple.1, alpha: 1)
            
            Menus.setAlpha(self.toppingArray, alpha: 0)
            Menus.setAlphaLabel(self.toppingLabels, alpha: 0)
            
            self.toppingArray[1].alpha = 1
            self.toppingArray[1].isEnabled = false
            
            self.backIcon.alpha = 1
        }
    }
    
    //MARK: - actions methods
    
    @objc private func stepperValueChanged(sender: UIStepper) {
        if identifiers.0.contains(sender.accessibilityIdentifier!) {
            for i in 0..<identifiers.0.count {
                if identifiers.0[i] == sender.accessibilityIdentifier {
                    if (identifiers.1[i] == 5 && sender.value > 0)       {
                        Others.errorAlert(self, title: "Maxed out", message: "maximum quantity achieved")
                                                                          break}
                    if (identifiers.1[i] == 0 && sender.value <= 0)      {break}
                    if sender.value > 0                                  {identifiers.1[i] += 1}
                    else                                                 {identifiers.1[i] -= 1}
                }
            }
        } else {
            identifiers.0.insert(sender.accessibilityIdentifier!, at: 0)
            identifiers.1.insert(1, at: 0)
        }
        identifiers = Others.removeZero(identifiers)
        if let parentVC = self.parent as? CreatePizza {
            parentVC.ingredients = identifiers
            parentVC.tableView.reloadData()
        }
        sender.value = 0
    }
    
    @IBAction func back(_ sender: UIButton) {
        if let parentVC = self.parent as? CreatePizza {
            parentVC.hideView(1)
            parentVC.ingredients = identifiers
        }
        Others.dismissChild(currentView: self)
    }
}
