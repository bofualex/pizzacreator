


import Foundation
import UIKit


class CreatePizza: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let manager = Manager()
    
    var ingredients: ([String], [Int]) = ([], [])
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet var navigationBar: UINavigationBar!
    @IBOutlet var background: UIImageView!
    
    @IBOutlet var toppingIcon: UIButton!
    @IBOutlet var toppingLabel: UILabel!
    
    @IBOutlet var saucesIcon: UIButton!
    @IBOutlet var saucesLabel: UILabel!
    
    @IBOutlet var doughIcon: UIButton!
    @IBOutlet var doughLabel: UILabel!
    
    //MARK: - instance methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView!.delegate = self
        tableView!.dataSource = self
        HideViews.transparentNav(navigationBar)
        hideView(1)
        tableView.alpha = 0.7
    }
    
    func calculatePrice() {
        var total: Double = 0
        total += Others.cellPriceFromArray(ingredients)
        priceLabel.text = " total      "  + String(total)

    }
    
    func hideView(_ alpha: CGFloat) {
        UIView .animate(withDuration: 0.3) {
            HideViews.alphaMenu(alpha, doughIcon: self.doughIcon, saucesIcon: self.saucesIcon, toppingIcon: self.toppingIcon, doughLabel: self.doughLabel, saucesLabel: self.saucesLabel, toppingLabel: self.toppingLabel, navigationBar: self.navigationBar)
            self.background.alpha = 1
        }
    }
    
    //MARK: - table methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ingredients.1.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "IngredientsCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CustomIngredientsCell
        let ingredient = ingredients.0[indexPath.row]
        let number = ingredients.1[indexPath.row]
        calculatePrice()
        cell.ingredientsLabel.text = "you have selected " + String(number) + ingredient
        cell.ingredientsLabel.textColor = UIColor.black
        cell.ingredientsLabel.font = UIFont.boldSystemFont(ofSize: 12)
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            ingredients.0.remove(at: indexPath.row)
            ingredients.1.remove(at: indexPath.row)
            calculatePrice()
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
    }
    
    //MARK: - actions methods
    
    @IBAction func refresh() {
        ingredients = ([], [])
        calculatePrice()
        tableView.reloadData()
    }
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        Others.dismissChild(currentView: self)
    }
    
    //MARK: - child methods

    @IBAction func save(_ sender: UIBarButtonItem) {
        guard ingredients.0.description.contains("dough") else {
            Others.errorAlert(self, title: "Incomplete Pizza", message: "you must have a dough")
            return
        }
        let saveAlert = UIAlertController(title: "Save your pizza for later use", message: nil, preferredStyle: UIAlertControllerStyle.alert)
        
        saveAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            self.manager.addPizzaModel(self.ingredients)
            Others.dismissChild(currentView: self)
        }))
        saveAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
        }))
        
        saveAlert.view.alpha = 0.7
        present(saveAlert, animated: true, completion: nil)
    }
    
    @IBAction func shoppingBasket(_ sender: UIButton) {
        ToSend.ingredients.append(ingredients)
        _ = Others.createChild(currentView: self, name: "ShoppingBasket") as? ShoppingBasket
    }
    
    @IBAction func toppingButtonTapped(_ sender: AnyObject) {
        hideView(0)
        let toppings = Others.createChild(currentView: self, name: "Toppings") as? Toppings
        toppings?.identifiers = ingredients
    }
    
    @IBAction func saucesButtonTapped(_ sender: AnyObject) {
        hideView(0)
        let sauces = Others.createChild(currentView: self, name: "Sauces") as? Sauces
        sauces?.identifiers = ingredients
    }
    
    @IBAction func doughButtonTapped(_ sender: AnyObject) {
        hideView(0)
        let dough = Others.createChild(currentView: self, name: "Doughs") as? Doughs
        dough?.identifiers = ingredients
    }
    
    @IBAction func fullScreenTable(_ sender: UITapGestureRecognizer) {
        hideView(0)
        background.alpha = 0.7
        tableView.alpha = 0
        let table = Others.createChild(currentView: self, name: "FullScreenTable") as? FullScreenTable
        table?.ingredients = ingredients
    }
    
}
