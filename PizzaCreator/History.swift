

import Foundation
import UIKit

class History: UIViewController, ManagerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    var history: [PizzaModel]?
    
    private let manager = Manager()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        manager.delegate = self
        manager.requestPizzaHistory()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number: Int!
        guard history != nil else { number = 1; return number }
        number = history!.count
        return number
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "HistoryCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! HistoryCell
        history = Others.sortHistory(&history!)
        let row = history![indexPath.row]
        if history != nil {
            cell.ingredientsLabel.text = Others.cellStringFromArrays((row.ingredients, row.quantity))
        } else {
            cell.ingredientsLabel.text = " You haven't saved anything "
        }
        cell.dateLabel.text = Others.formatDate(row.date)
        cell.activityMonitor?.stopAnimating()
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Delete", handler: { (action, indexPath) -> Void in
            self.manager.deleteEntry(self.history![(indexPath as NSIndexPath).row])
            self.manager.requestPizzaHistory()
        })
        
        let orderAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Order it", handler: { (action, indexPath) -> Void in
            
            let row = self.history![(indexPath as NSIndexPath).row]
            ToSend.ingredients.append((row.ingredients , row.quantity))
 
            _ = (Others.createChild(currentView: self, name: "ShoppingBasket") as? ShoppingBasket)!
        })
        deleteAction.backgroundColor = UIColor.red
        orderAction.backgroundColor = UIColor.orange
        return [orderAction, deleteAction]
    }
    
    //MARK: Action methods
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        Others.dismissChild(currentView: self)
    }
    
    //MARK: Delegate method
    
    func pizzaHistory(_ pizza: [PizzaModel]) {
        history = pizza
        DispatchQueue.main.async { [weak self] in
        self!.tableView.reloadData()
        }
    }
}
