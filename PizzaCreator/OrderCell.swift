

import UIKit

class OrderCell: UITableViewCell {
    
    @IBOutlet weak var ingredientsLabel: UILabel!
    @IBOutlet weak var scroll: UIScrollView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        scroll.contentSize.height = ingredientsLabel.bounds.height
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
