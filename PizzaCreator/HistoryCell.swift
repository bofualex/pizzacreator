

import UIKit

class HistoryCell: UITableViewCell {
    
    @IBOutlet weak var ingredientsLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var activityMonitor: UIActivityIndicatorView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        activityMonitor?.startAnimating()
        ingredientsLabel.adjustsFontSizeToFitWidth = true
        ingredientsLabel.isUserInteractionEnabled = false
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
