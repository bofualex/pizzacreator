

import Foundation
//import Alamofire
//
////MARK: protocol
//
//@objc protocol ApiRequestDelegate: class{
//    optional func detailRequestDelegate(response: [Weather])
//    optional func detailDailyRequest(response: [WeeklyWeather])
//}
//
////MARK: class
//
//class ApiRequest: NSObject{
//    
//    weak var delegate: ApiRequestDelegate?
//    private let apiKey = "6638a13657b81ecbe08a47749ecfa9b7"
//    
//    //MARK: request methods
//    
//    func requestDailyWeather(string: String) {
//        let baseURL = NSURL(string: "https://api.forecast.io/forecast/\(apiKey)/")
//        let forecastURL = NSURL(string: string, relativeToURL: baseURL)
//        
//        Alamofire.request(.GET, forecastURL!, parameters: ["units": "si", "exclude": "currently,minutely,alerts,flags"]).responseJSON {
//            response in
//            if let JSON = response.result.value {
//                let jsonDict = JSON.valueForKey("hourly")
//                let todayWeather = (jsonDict!.valueForKey("data") as! [NSDictionary]).map { Weather(apparentTemperature: $0["apparentTemperature"] as! NSNumber, cloudCover: $0["cloudCover"] as! NSNumber, humidity: $0["humidity"] as! NSNumber, icon: $0["icon"] as! String, summary: $0["summary"] as! String, windSpeed: $0["windSpeed"] as! NSNumber, time: $0["time"] as! NSNumber)}
//                self.delegate?.detailRequestDelegate!(todayWeather)
//                let jsonDict1 = JSON.valueForKey("daily")
//                let dailyWeather = (jsonDict1!.valueForKey("data") as! [NSDictionary]).map { WeeklyWeather(apparentTemperatureMax: $0["apparentTemperatureMax"] as! NSNumber, apparentTemperatureMin: $0["apparentTemperatureMin"] as! NSNumber, cloudCover: $0["cloudCover"] as! NSNumber, humidity: $0["humidity"] as! NSNumber, icon: $0["icon"] as! String, summary: $0["summary"] as! String, windSpeed: $0["windSpeed"] as! NSNumber, time: $0["time"] as! NSNumber, sunsetTime: $0["sunsetTime"] as! NSNumber, sunriseTime: $0["sunriseTime"] as! NSNumber, temperatureMaxTime: $0["temperatureMaxTime"] as! NSNumber, temperatureMinTime: $0["temperatureMinTime"] as! NSNumber, precipProbability: $0["precipProbability"] as! NSNumber)}
//                self.delegate?.detailDailyRequest!(dailyWeather)
//            }
//        }
//    }
//    
//}
//
//
