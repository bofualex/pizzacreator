

import Foundation
import UIKit

class Menus: NSObject {
    
    
    //MARK: - set view methods
    
    class func setAlpha(_ button1: [UIButton], alpha: CGFloat) {
        for i in 0..<button1.count {
            button1[i].alpha = alpha
        }
    }
    
    class func setAlphaLabel(_ label: [UILabel], alpha: CGFloat) {
        for i in 0..<label.count {
            label[i].alpha = alpha
        }
    }
    
    class func setEnabled(_ button1: [UIButton], enabled: Bool) {
        for i in 0..<button1.count {
            button1[i].isEnabled = enabled
        }
    }
    
    //MARK: - create buttons and labels

    class func generateButtons(_ view: UIView, buttonType: String, backColor: UIColor) -> [UIButton] {
        var button: [UIButton] = []
        let resourcePath = Bundle.main.resourcePath!
        let imgName = "Images.xcassets"
        let path = resourcePath + "/" + imgName
        let fm = FileManager.default
        let items = try! fm.contentsOfDirectory(atPath: path)
        var objects: [String] = []
        
        for item in items {
            if item.hasPrefix(buttonType) {
                objects.append(item)
            }
        }
        var format: [String] = []
        for item in objects {
            format.append((item.components(separatedBy: CharacterSet.punctuationCharacters).first)!)
        }
        for i in 0..<format.count {
            let name = UIButton()
            name.translatesAutoresizingMaskIntoConstraints = false
            var array: [UIImage] = []
            for item in format
            {
                array.append(UIImage(named: item)!)
            }
            name.setImage(array[i], for: UIControlState())
            name.backgroundColor = backColor
            name.layer.cornerRadius = 20
            name.tag = i
            name.alpha = 0
            view.addSubview(name)
            let width = NSLayoutConstraint(item: name, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
            let height = NSLayoutConstraint(item: name, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
            view.addConstraints([width, height])
            button.append(name)
        }
        return button
    }
    
    class func createAboutButtons(_ view: UIView) -> [UIButton] {
        var button: [UIButton] = []
        let string: [String] = [" Contact ", " About ", " Review us "]
        var x: CGFloat = 0
        for i in 0..<string.count {
            let name = UIButton()
            name.translatesAutoresizingMaskIntoConstraints = false
            name.setTitle(string[i], for: UIControlState())
            name.backgroundColor = UIColor.black
            name.layer.cornerRadius = 14
            name.alpha = 0
            view.addSubview(name)
            let pinLeft = NSLayoutConstraint(item: name, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: 250)
            let pinTop = NSLayoutConstraint(item: name, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: 70 + x)
            let width = NSLayoutConstraint(item: name, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 100)
            let height = NSLayoutConstraint(item: name, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 30)
            view.addConstraints([pinLeft, pinTop, width, height])
            button.append(name)
            x += 40
        }
        return button
    }
    
    class func createLabels(_ view: UIView, names: [String], backColor: UIColor) -> [UILabel] {
        var labelArray: [UILabel] = []
        var x: CGFloat = 0
        for i in 0..<names.count {
            let labels = UILabel()
            labels.translatesAutoresizingMaskIntoConstraints = false
            labels.text = names[i]
            labels.adjustsFontSizeToFitWidth = true
            labels.font = UIFont.boldSystemFont(ofSize: 18)
            labels.textColor = backColor
            labels.clipsToBounds = true
            labels.layer.cornerRadius = 14
            view.addSubview(labels)
            
            let height = NSLayoutConstraint(item: labels, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 30)
            view.addConstraints([height])
            labelArray.append(labels)
            x += 40
        }
        return labelArray
    }
    
    //MARK: - tuple methods
    
    class func generateIngredientsTuple(_ view: UIView, buttonType: String, names: [String], backColor: UIColor, constant: CGFloat) -> ([UIButton], [UIStepper], [UILabel]) {
        var button: [UIButton] = []
        var stepperArray: [UIStepper] = []
        var labelArray: [UILabel] = []

        let resourcePath = Bundle.main.resourcePath!
        let imgName = "Images.xcassets"
        let path = resourcePath + "/" + imgName
        let fm = FileManager.default
        let items = try! fm.contentsOfDirectory(atPath: path)
        var objects: [String] = []
        
        for item in items {
            if item.hasPrefix(buttonType) {
                objects.append(item)
            }
        }
        var format: [String] = []
        for item in objects {
            format.append((item.components(separatedBy: CharacterSet.punctuationCharacters).first)!)
        }
        for i in 0..<format.count {
            let name = UIButton()
            name.translatesAutoresizingMaskIntoConstraints = false
            var array: [UIImage] = []
            for item in format
            {
                array.append(UIImage(named: item)!)
            }
            name.setImage(array[i], for: UIControlState())
            name.backgroundColor = backColor
            name.layer.cornerRadius = 20
            name.tag = i
            name.alpha = 0
            view.addSubview(name)
            
            let labels = UILabel()
            labels.translatesAutoresizingMaskIntoConstraints = false
            labels.text = names[i]
            labels.adjustsFontSizeToFitWidth = true
            labels.font = UIFont.boldSystemFont(ofSize: 18)
            labels.textColor = backColor
            view.addSubview(labels)
            
            let stepper = UIStepper()
            stepper.translatesAutoresizingMaskIntoConstraints = false
            stepper.backgroundColor = backColor
            stepper.tintColor = .black
            stepper.clipsToBounds = true
            stepper.layer.cornerRadius = 20
            stepper.stepValue = 1
            stepper.wraps = false
            stepper.autorepeat = true
            stepper.minimumValue = 0
            stepper.maximumValue = 5
            stepper.accessibilityIdentifier = labels.text
            view.addSubview(stepper)
            
            let widthButton = NSLayoutConstraint(item: name, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
            let heightButton = NSLayoutConstraint(item: name, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
            let pinRightButton = NSLayoutConstraint(item: name, attribute: .rightMargin, relatedBy: .equal, toItem: view, attribute: .rightMargin, multiplier: 1.0, constant: constant)

            let heightLabel = NSLayoutConstraint(item: labels, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 30)

            let heightStepper = NSLayoutConstraint(item: stepper, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40)
            let widthStepper = NSLayoutConstraint(item: stepper, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 70)

            let pinRightLabel = NSLayoutConstraint(item: labels, attribute: .rightMargin, relatedBy: .equal, toItem: view, attribute: .rightMargin, multiplier: 1.0, constant: pinRightButton.constant - 45)
            
            let pinRightStepper = NSLayoutConstraint(item: stepper, attribute: .rightMargin, relatedBy: .equal, toItem: view, attribute: .rightMargin, multiplier: 1.0, constant: pinRightLabel.constant - 125)

            view.addConstraints([widthButton, heightButton, pinRightButton, heightLabel, heightStepper, widthStepper, pinRightLabel, pinRightStepper])
            labelArray.append(labels)
            stepperArray.append(stepper)
            button.append(name)
        }
        return (button, stepperArray, labelArray)
    }
    
    class func setAlphaTuple(_ buttons: [UIButton], labels: [UILabel], steppers: [UIStepper], alpha: CGFloat) {
        for i in 0..<buttons.count {
            buttons[i].alpha = alpha
            labels[i].alpha = alpha
            steppers[i].alpha = alpha
        }
    }
}

