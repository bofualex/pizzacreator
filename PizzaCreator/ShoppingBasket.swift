

import Foundation
import UIKit
import MessageUI

class ShoppingBasket: UIViewController, UITableViewDelegate, UITableViewDataSource, MFMessageComposeViewControllerDelegate  {
    
    @IBOutlet weak var pizzaTable: UITableView!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var pricePerPizza: UILabel!
    
    let messageVC = MFMessageComposeViewController()
    var ingredients: [([String], [Int])]?
    
    static let sharedInstance = ShoppingBasket()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ingredients = ToSend.ingredients
        pizzaTable.delegate = self
        pizzaTable.dataSource = self
        messageVC.messageComposeDelegate = self
    }
    
    //MARK: - instance methods
    
    private func calculateTotal() {
        var total: Double = 0
        var perPizza: [Double] = []
        if ingredients != nil {
            for i in 0..<ingredients!.count {
            total += Others.cellPriceFromArray(ingredients![i])
            perPizza.append(Others.cellPriceFromArray(ingredients![i]))
            }
        }
        var string: String = ""
        for i in 0..<perPizza.count {
            string = string + " pizza  \(i+1)     \(perPizza[i]) \n"
        }
        pricePerPizza.text = string
        totalPrice.text = " total      "  + String(total)
    }
    
    // MARK: - table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number: Int! = 1
        guard ingredients != nil else { return number }
        number = ingredients!.count
        return number
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "OrderCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! OrderCell
        cell.ingredientsLabel.text = Others.cellStringFromArrays(ingredients![(indexPath as NSIndexPath).row])
        calculateTotal()
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: "Delete", handler: { (action, indexPath) -> Void in
            self.ingredients?.remove(at: (indexPath as NSIndexPath).row)
            self.calculateTotal()
            self.pizzaTable.reloadData()
        })
        let addMoreAction = UITableViewRowAction(style: UITableViewRowActionStyle.default, title: " Add \n More", handler: { (action, indexPath) -> Void in
            ToSend.ingredients.removeLast()
            Others.dismissChild(currentView: self)
        })
        
        deleteAction.backgroundColor = UIColor.red
        addMoreAction.backgroundColor = UIColor.brown
        return [deleteAction, addMoreAction]
    }
    
    //MARK: - actions methods
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }

    @IBAction func continueShopping(_ sender: UIButton) {
        if let pvc = self.parent as? CreatePizza {
            pvc.ingredients.0.removeAll()
            pvc.ingredients.1.removeAll()
            pvc.priceLabel.text = ""
            pvc.tableView.reloadData()
        }
        ToSend.ingredients = ingredients!
        Others.dismissChild(currentView: self)
    }
    
    //MARK: - sms methods
    
    @IBAction func sendMessage(sender: AnyObject) {
        for index in ingredients!.indices {
            guard ingredients![index].0.description.contains("dough") else {
                Others.errorAlert(self, title: "Incomplete Pizza", message: "you must have a dough")
                return
                }
            }
            messageVC.body = ToSend.ingredients.description
            messageVC.recipients = []
            present(messageVC, animated: true, completion: nil)
        }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch result {
        case .cancelled:
            self.dismiss(animated: true, completion: nil)  //to do error handling
        case .failed:
            self.dismiss(animated: true, completion: nil)
        case .sent:
            self.dismiss(animated: true, completion: nil)
        }
    }
}
