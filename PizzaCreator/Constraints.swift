

import Foundation
import UIKit

class Constraints: NSObject {
    
    
    //MARK: - set and reset constraints
    
    class func setConstraints(_ constraint: [NSLayoutConstraint], radius: CGFloat, x: CGFloat, y: CGFloat) {
        for i in 0..<constraint.count {
            let z = CGFloat((constraint.count + i))
            constraint[i].constant = radius * -sin(0) + x + y*z
        }
    }
    
    class func resetConstraints(_ constraint: [NSLayoutConstraint]) {
        for i in 0..<constraint.count {
            constraint[i].constant = 40
        }
    }
    
    class func generateConstraintsLeft(_ view: UIView, button1: [AnyObject], constant: CGFloat) -> [NSLayoutConstraint] {
        var constraintsLeft: [NSLayoutConstraint] = []
        for i in 0..<button1.count {
            let pinLeft = NSLayoutConstraint(item: button1[i], attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: constant)
            constraintsLeft.append(pinLeft)
            view.addConstraint(pinLeft)
        }
        return constraintsLeft
    }
    
    class func generateConstraintsRight(_ view: UIView, button1: [AnyObject], constant: CGFloat) -> [NSLayoutConstraint] {
        var constraintsRight: [NSLayoutConstraint] = []
        for i in 0..<button1.count {
            let pinRight = NSLayoutConstraint(item: button1[i], attribute: .rightMargin, relatedBy: .equal, toItem: view, attribute: .rightMargin, multiplier: 1.0, constant: constant)
            constraintsRight.append(pinRight)
            view.addConstraint(pinRight)
        }
        return constraintsRight
    }
    
    //MARK: - generate constraints
    
    class func generateConstraintsBottom(_ view: UIView, button1: [AnyObject], constant: CGFloat) -> [NSLayoutConstraint] {
        var constraintsBottom: [NSLayoutConstraint] = []
        for i in 0..<button1.count {
            let pinBottom = NSLayoutConstraint(item: button1[i], attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1.0, constant: constant)
            constraintsBottom.append(pinBottom)
            view.addConstraint(pinBottom)
        }
        return constraintsBottom
    }
    
    class func generateConstraintsTop(_ view: UIView, button1: [AnyObject], constant: CGFloat) -> [NSLayoutConstraint] {
        var constraintsTop: [NSLayoutConstraint] = []
        for i in 0..<button1.count {
            let pinTop = NSLayoutConstraint(item: button1[i], attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: constant)
            constraintsTop.append(pinTop)
            view.addConstraint(pinTop)
        }
        return constraintsTop
    }
    
    //MARK: - set reset and generate tuple constraints
    
    class func setConstraintsTuple(_ constraint1: [NSLayoutConstraint], constraint2: [NSLayoutConstraint], constraint3: [NSLayoutConstraint], x: CGFloat, y: CGFloat) {
        for i in 0..<constraint1.count {
            let z = CGFloat((constraint1.count + i))
            constraint1[i].constant = constraint1[i].constant + x + y*z
            constraint2[i].constant = constraint2[i].constant + x + y*z
            constraint3[i].constant = constraint3[i].constant + x + y*z
        }
    }
    
    class func resetConstraintsTuple(_ constraint: [NSLayoutConstraint], constraint1: [NSLayoutConstraint], constraint2: [NSLayoutConstraint]) {
        for i in 0..<constraint.count {
            constraint[i].constant = 40
            constraint1[i].constant = 35
            constraint2[i].constant = 45
        }
    }
    
    class func generateConstraintsTopTuple(_ view: UIView, button: [UIButton], labels: [UILabel], steppers: [UIStepper], constant: CGFloat) -> ([NSLayoutConstraint], [NSLayoutConstraint], [NSLayoutConstraint]) {
        var constrains: ([NSLayoutConstraint], [NSLayoutConstraint], [NSLayoutConstraint]) = ([], [], [])
        for i in 0..<button.count {
            let pinTopButton = NSLayoutConstraint(item: button[i], attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: constant)
            let pinTopLabel = NSLayoutConstraint(item: labels[i], attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: constant)
            let pinTopStepper = NSLayoutConstraint(item: steppers[i], attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1.0, constant: constant)
            constrains.0.append(pinTopButton)
            constrains.1.append(pinTopLabel)
            constrains.2.append(pinTopStepper)
            view.addConstraints([pinTopButton, pinTopLabel, pinTopStepper])
        }
        return constrains
    }
}
