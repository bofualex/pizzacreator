

import Foundation
import UIKit


class Others: NSObject {
    
    
    class func createChild(currentView: UIViewController, name: String) -> UIViewController {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: name)
        currentView.addChildViewController(viewController)
        currentView.view.addSubview(viewController.view)
        let viewsDict = ["child" : viewController.view] as [String: AnyObject]
        currentView.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[child]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict))
        currentView.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[child]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict))
        viewController.didMove(toParentViewController: currentView)
        return viewController
    }
    
    
    class func dismissChild(currentView: UIViewController) {
        currentView.willMove(toParentViewController: nil)
        currentView.view.removeFromSuperview()
        currentView.removeFromParentViewController()
    }
    
    
    class func cellPriceFromArray(_ price: ([String], [Int])) -> Double {
        var total: Double = 0
        var totalArray: [String: Double] = [:]
        for (item, index) in Strings.meatString   { totalArray[item] = index }
        for (item, index) in Strings.cheeseString { totalArray[item] = index }
        for (item, index) in Strings.veggString   { totalArray[item] = index }
        for (item, index) in Strings.saucesString { totalArray[item] = index }
        for (item, index) in Strings.doughsString { totalArray[item] = index }
        for (item, index) in Strings.exoticString { totalArray[item] = index }
        for i in 0..<price.1.count {
            for (key, value) in totalArray {
                if price.0[i].contains(key) {
                    total += Double(price.1[i]) * value
                }
            }
        }
        return total
    }
    
    class func removeZero(_ ingredients: ([String], [Int])) -> ([String], [Int])  {
        var ingredients = ingredients
        for _ in ingredients.1.indices {
            if let index = ingredients.1.index(of: 0) {
                ingredients.0.remove(at: index)
                ingredients.1.remove(at: index)
            }
        }
        return ingredients
    }
    
    
    class func sortAlph(_ labels: [String: Double]) -> [String] {
        var sortedLabels: [String]
        sortedLabels = labels.keys.sorted { $0 < $1 }
        return sortedLabels
    }
    
    class func sortHistory(_ history: inout [PizzaModel]) -> [PizzaModel] {
        history = history.sorted { $0.date > $1.date }
        return history
    }
    
    class func generateRandomColor() -> UIColor {
        let hue : CGFloat = CGFloat(arc4random() % 256) / 256
        let saturation : CGFloat = CGFloat(arc4random() % 128) / 256 + 0.5
        let brightness : CGFloat = CGFloat(arc4random() % 128) / 256 + 0.5 
        
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1)
    }
    
    
    class func formatDate(_ date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current
        dateFormatter.dateStyle = DateFormatter.Style.medium
        let convertedDate = dateFormatter.string(from: date)
        return convertedDate
    }
    
    
    class func cellStringFromArrays(_ pizza: ([String], [Int])) -> String {
        var newString: String = ""
        for i in 0..<pizza.0.count  {
            newString += String(pizza.1[i]) + " x " + pizza.0[i] + "\n"
        }
        return newString
    }

    
    class func errorAlert(_ currentView: UIViewController, title: String, message: String) {
        let errorAlert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        errorAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
        }))
        
        errorAlert.view.alpha = 0.7
        currentView.present(errorAlert, animated: true, completion: nil)
    }

}
