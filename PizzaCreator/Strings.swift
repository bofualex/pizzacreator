

import Foundation

public enum Strings {
    
    static var mainMenuMainScreen: [String] = [" Choose from our menu ", " Create your pizza ", " Special offers ", " Shopping basket ", " History "]
    static var toppingString: [String] = [" cheese ", " exotic ", " meats ", " veggies "]
    static var meatString: [String: Double] = [" chicken ": 0.7, " beef ": 0.9, " fish ": 0.9," pork ": 0.7]
    static var veggString: [String: Double] = [" corn ": 0.2, " fungi ": 0.4, " olives ": 0.4, " onion ": 0.2, " rucola ": 0.3, " tomato ": 0.3]
    static var cheeseString: [String: Double] = [" mozzarella ": 0.7, " ceddar ": 0.7, " parmesan ": 0.9, " feta ": 0.5, " provolone ": 0.9]
    static var exoticString: [String: Double] = [" calamari ": 0.75, " pinneapple ": 0.6, " pistachio ": 0.8, " shrimp ": 0.75]
    static var saucesString: [String: Double] = [" cream ": 0.5, " garlic ": 0.4, " olive oil": 0.6, " tomato hot ": 0.4, " tomato sw ": 0.4]
    static var doughsString: [String: Double] = [" slim and crunchy ": 1.7, " with a bit of cheese ": 2.4, " puffy nipples ": 1.7, " classic is forever ": 1.5]
}


public enum Buttons {
    
    static var cheeseButton: String = "chs"
    static var meatButton: String = "meat"
    static var veggButton: String = "vegg"
    static var exoticButton: String = "exo"
    static var toppingButton: String = "topping"
    static var saucesButton: String = "sauces"
    static var doughsButton: String = "doughs"
    static var mainMenuMainScreenButton: String = "mainMenu"
    
}

