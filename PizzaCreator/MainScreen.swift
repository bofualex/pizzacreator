

import UIKit


class MainScreen: UIViewController {
    
    @IBOutlet var more: UIButton!
    @IBOutlet var welcome: UIButton!
    @IBOutlet var view1: UIImageView!
    
    private var menuArray: [UIButton] = []
    private var menuConstraintsTop: [NSLayoutConstraint] = []
    private var menuConstraintsLeft: [NSLayoutConstraint] = []
    
    private var menuLabelsArray: [UILabel] = []
    private var menuLabelsConstraintsTop: [NSLayoutConstraint] = []
    private var menuLabelsConstraintsLeft: [NSLayoutConstraint] = []
    
    private var moreButtons: [UIButton] = []
    
    //MARK: - instance methods

    override func viewDidLoad() {
        super.viewDidLoad()
        initAll()
        hideView()
    }
    
    private func initAll() {
        more?.addTarget(self, action: #selector(animateButtonsRight), for: .touchUpInside)
        welcome.addTarget(self, action: #selector(animateButtonsLeft), for: .touchUpInside)

        menuArray = Menus.generateButtons(view, buttonType: Buttons.mainMenuMainScreenButton, backColor: .green)
        menuLabelsArray = Menus.createLabels(view, names: Strings.mainMenuMainScreen, backColor: .green)
        moreButtons = Menus.createAboutButtons(view)
        
        menuConstraintsTop = Constraints.generateConstraintsTop(view, button1: menuArray, constant: 40)
        menuConstraintsLeft = Constraints.generateConstraintsLeft(view, button1: menuArray, constant: 40)
        menuLabelsConstraintsTop = Constraints.generateConstraintsTop(view, button1: menuLabelsArray, constant: 40)
        menuLabelsConstraintsLeft = Constraints.generateConstraintsLeft(view, button1: menuLabelsArray, constant: 40)
        
        menuArray[1].addTarget(self, action: #selector(createYourPizza), for: .touchUpInside)
        menuArray[3].addTarget(self, action: #selector(shoppingBasket), for: .touchUpInside)
        menuArray[4].addTarget(self, action: #selector(historyPizza), for: .touchUpInside)
        
        moreButtons[0].addTarget(self, action: #selector(contact), for: .touchUpInside)
        moreButtons[1].addTarget(self, action: #selector(about), for: .touchUpInside)
        moreButtons[2].addTarget(self, action: #selector(reviewus), for: .touchUpInside)
    }
    
    private func hideView() {
        Constraints.resetConstraints(menuConstraintsTop)
        Constraints.resetConstraints(menuLabelsConstraintsLeft)
        Constraints.resetConstraints(menuLabelsConstraintsTop)
        Constraints.resetConstraints(menuConstraintsLeft)
        Menus.setAlpha(self.menuArray, alpha: 0)
        Menus.setAlphaLabel(self.menuLabelsArray, alpha: 0)
        Menus.setAlpha(self.moreButtons, alpha: 0)
        
        UIView .animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            self.more.alpha = 1
            self.welcome.alpha = 1
            self.view1.alpha = 1
        }
    }
    
    //MARK: - menus
    
    @objc private func animateButtonsLeft() {
        Constraints.setConstraints(menuConstraintsTop, radius: 0, x: -150, y: 45)
        Constraints.setConstraints(menuConstraintsLeft, radius: 160, x: 260, y: -28)
        Constraints.setConstraints(menuLabelsConstraintsTop, radius: 0, x: -150, y: 45)
        Constraints.setConstraints(menuLabelsConstraintsLeft, radius: 160, x: 300, y: -28)

        UIView .animate(withDuration: 0.3) {
            Menus.setAlpha(self.menuArray, alpha: 1)
            Menus.setAlphaLabel(self.menuLabelsArray, alpha: 1)
            self.view.layoutIfNeeded()
            
            self.welcome.alpha = 0
            self.more.alpha = 0
            self.view1.alpha = 0.7
        } 
    }
    
    @objc private func animateButtonsRight() {
        UIView .animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            Menus.setAlpha(self.menuArray, alpha: 0)
            Menus.setAlphaLabel(self.menuLabelsArray, alpha: 0)
            Menus.setAlpha(self.moreButtons, alpha: 1)

            self.welcome.alpha = 0
            self.more.alpha = 0
            self.view1.alpha = 0.7
        }
    }
    
    //MARK: - actions methods
    
    @IBAction func tappedView() {
        hideView()
    }

    //MARK: - right menu classes
    
    func createYourPizza() {
        hideView()
        _ = Others.createChild(currentView: self, name: "CreatePizza") as? CreatePizza
    }

    func shoppingBasket() {
        hideView()
        _ = (Others.createChild(currentView: self, name: "ShoppingBasket") as? ShoppingBasket)!
    }

    func historyPizza() {
        hideView()
        _ = Others.createChild(currentView: self, name: "History") as? History
    }
    
    //MARK: - left menu classes methods

    func contact() {
        hideView()
        _ = Others.createChild(currentView: self, name: "Contact") as? Contact
    }
    
    func about() {
        hideView()
        _ = Others.createChild(currentView: self, name: "About") as? About
    }
    
    func reviewus() {
        hideView()
        _ = Others.createChild(currentView: self, name: "ReviewUs") as? ReviewUs
    }

}

