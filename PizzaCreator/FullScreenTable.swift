

import UIKit

class FullScreenTable: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var ingredients: ([String], [Int]) = ([], [])
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView!.delegate = self
        tableView!.dataSource = self
        view.backgroundColor = UIColor.clear
        tableView.backgroundColor = UIColor.clear
        tableView.alpha = 0.7
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - tableview data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ingredients.1.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "IngredientsCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! CustomIngredientsCell
        let ingredient = ingredients.0[indexPath.row]
        let number = ingredients.1[indexPath.row]
        cell.ingredientsLabel.text = "you have selected " + String(number) + ingredient
        cell.ingredientsLabel.textColor = UIColor.black
        cell.ingredientsLabel.font = UIFont.boldSystemFont(ofSize: 18)
        cell.alpha = 0.5
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            ingredients.0.remove(at: indexPath.row)
            ingredients.1.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
    }
    
    //MARK: - action methods

    @IBAction func back(_ sender: UITapGestureRecognizer) {
        if let parentVC = self.parent as? CreatePizza {
            parentVC.ingredients = ingredients
            parentVC.tableView.reloadData()
            parentVC.hideView(1)
            parentVC.tableView.alpha = 0.7
        }
        Others.dismissChild(currentView: self)
    }

}
