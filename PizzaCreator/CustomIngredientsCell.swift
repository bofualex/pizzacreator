


import UIKit

class CustomIngredientsCell: UITableViewCell {

    @IBOutlet weak var ingredientsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ingredientsLabel.font = UIFont.boldSystemFont(ofSize: 10)
        ingredientsLabel.adjustsFontSizeToFitWidth = true
        ingredientsLabel.isUserInteractionEnabled = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
