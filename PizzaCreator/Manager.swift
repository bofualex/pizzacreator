

import Foundation
import UIKit
import CoreData


protocol ManagerDelegate: class {
    func pizzaHistory(_ pizza: [PizzaModel])
}

class Manager: NSObject {
    
    weak var delegate: ManagerDelegate?
    let context = (UIApplication.shared.delegate as? AppDelegate)!.managedObjectContext
    let request = PizzaModel.fetchRequest()
    let queue = DispatchQueue(label: "coredata")
    
    //MARK: - coredata methods
    
    
    func addPizzaModel(_ ingredients: ([String], [Int])) {
        queue.async(execute: { [weak self] in
            
            let pizzaModel = NSEntityDescription.insertNewObject(forEntityName: "PizzaModel", into: self!.context) as? PizzaModel
            
            for ingredient in ingredients.0 {
                pizzaModel!.ingredients.append(ingredient)
            }
            for quantity in ingredients.1 {
                pizzaModel?.quantity.append(quantity)
            }
            pizzaModel!.date = Date()
            do {
                try self!.context.save()
            } catch {
                print(error)
            }
            
        })
    }
    
    func requestPizzaHistory() {
        queue.async(execute: { [weak self] in
            
            do {
                let results = try self!.context.fetch(self!.request)
                self!.delegate?.pizzaHistory(results as! [PizzaModel])
            } catch {
                print(error)
            }
        })
    }
    
    func deleteEntry(_ object: PizzaModel) {
        queue.async(execute: { [weak self] in

        self!.context.delete(object)
        do {
            try self!.context.save()
        } catch {
            print(error)
        }
        })
    }
    
}
