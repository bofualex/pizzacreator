


import Foundation
import UIKit

class Doughs: UIViewController {
    
    @IBOutlet weak var switchSelect: UISwitch!
    @IBOutlet weak var switchTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var backIcon: UIButton!
    var identifiers: ([String], [Int]) = ([], [])
    
    private var doughsButtons: [UIButton] = []
    private var doughsLabels: [UILabel] = []
    private var doughsButtonsConstraintsTop: [NSLayoutConstraint] = []
    private var doughsButtonsConstraintsRight: [NSLayoutConstraint] = []
    private var doughsLabelsConstraintsTop: [NSLayoutConstraint] = []
    private var doughsLabelsConstraintsRight: [NSLayoutConstraint] = []
    
    //MARK: - instance methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        initButtons()
        resetConstraints()
        animateButtonsRight()
    }
    
    private func initButtons() {
        doughsButtons = Menus.generateButtons(view, buttonType: Buttons.doughsButton, backColor: #colorLiteral(red: 0.09014640634, green: 0.9803921569, blue: 0.9793165528, alpha: 1))
        doughsButtonsConstraintsTop = Constraints.generateConstraintsTop(view, button1: doughsButtons, constant: 40)
        doughsButtonsConstraintsRight = Constraints.generateConstraintsRight(view, button1: doughsButtons, constant: -10)
        doughsLabels = Menus.createLabels(view, names: Others.sortAlph(Strings.doughsString), backColor: #colorLiteral(red: 0.09014640634, green: 0.9803921569, blue: 0.9793165528, alpha: 1))
        doughsLabelsConstraintsTop = Constraints.generateConstraintsTop(view, button1: doughsLabels, constant: 40)
        doughsLabelsConstraintsRight = Constraints.generateConstraintsRight(view, button1: doughsLabels, constant: -50)
        
        backIcon.addTarget(self, action: #selector(animateButtonsRight), for: .touchUpInside)
        for i in 0..<doughsButtons.count{
            doughsButtons[i].addTarget(self, action: #selector(buttonClicked), for: .touchUpInside)
        }
    }
    
    private func resetConstraints() {
        Constraints.resetConstraints(doughsButtonsConstraintsTop)
        Constraints.resetConstraints(doughsLabelsConstraintsTop)
        Menus.setAlpha(doughsButtons, alpha: 0)
        Menus.setAlphaLabel(doughsLabels, alpha: 0)
        backIcon.alpha = 0
        switchTopConstraint.constant = 8
        switchSelect.alpha = 0
    }
    
    //MARK: - menu methods
    
    @objc private func animateButtonsRight() {
        resetConstraints()
        Constraints.setConstraints(doughsButtonsConstraintsTop, radius: 0, x: 0, y: 45)
        Constraints.setConstraints(doughsLabelsConstraintsTop, radius: 0, x: -5, y: 45)
        UIView .animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            Menus.setEnabled(self.doughsButtons, enabled: true)
            Menus.setAlpha(self.doughsButtons, alpha: 1)
            Menus.setAlphaLabel(self.doughsLabels, alpha: 1)
            self.backIcon.alpha = 0
        }
    }
    
    
    //MARK: - action methods
    
    @objc private func buttonClicked(_ sender: UIButton) {
        for i in 0..<doughsButtons.count {
            if doughsButtons[i].tag == sender.tag {
                resetConstraints()
                switchSelect.alpha = 1
                backIcon.alpha = 1
                doughsButtons[i].alpha = 1
                doughsLabels[i].alpha = 1
                Constraints.setConstraints(doughsButtonsConstraintsTop, radius: 0, x: 0, y: 45)
                Constraints.setConstraints(doughsLabelsConstraintsTop, radius: 0, x: -5, y: 45)
                switchTopConstraint.constant = doughsButtonsConstraintsTop[i].constant - 55
                switchSelect.addTarget(self, action: #selector(switchMoved), for: .valueChanged)
            }
        }
        switchSelect.setOn(false, animated: true)
    }
    
    @objc private func switchMoved(_ sender: UISwitch) {
        sender.isOn = true
        for i in 0..<Strings.doughsString.count {
        if let index = identifiers.0.index(of: " dough " + Others.sortAlph(Strings.doughsString)[i]) {
                identifiers.0.remove(at: index)
                identifiers.1.remove(at: index)
            }
        }
        for i in 0..<doughsButtons.count {
            if switchTopConstraint.constant == doughsButtonsConstraintsTop[i].constant - 55 {
                identifiers.0.insert(" dough " + doughsLabels[i].text!, at: 0)
                identifiers.1.insert(1, at: 0)
                if let parentVC = self.parent as? CreatePizza {
                    parentVC.ingredients = identifiers
                    parentVC.tableView.reloadData()
                }
            }
        }
        animateButtonsRight()
    }
    
    @IBAction func back(_ sender: UIButton) {
        if let parentVC = self.parent as? CreatePizza {
            parentVC.hideView(1)
            parentVC.ingredients = identifiers
        }
        Others.dismissChild(currentView: self)
    }
}
