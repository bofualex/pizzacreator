


import Foundation
import UIKit
import MapKit


class Contact: UIViewController, MKMapViewDelegate {
    
    
    @IBOutlet var mapView:MKMapView!
    
    //MARK: - instance methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showOnMap()
    }
 
    private func showOnMap() {
        let annotation = MKPointAnnotation()
        let coordinates = CLLocation(latitude: 25.5667, longitude: 47.5333)
        annotation.title = "Pizza factory"
        annotation.coordinate = coordinates.coordinate
        self.mapView.showAnnotations([annotation], animated: true)
        self.mapView.selectAnnotation(annotation, animated: true)
        mapView.showsCompass = true
        mapView.showsScale = true
        mapView.delegate = self
    }
    
    // MARK: - delegate methods
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "MyPin"
        
        var annotationView:MKPinAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
        }
        annotationView?.pinTintColor = UIColor.orange
        
        return annotationView
    }

    
    @IBAction func back(_ sender: UIButton) {
        Others.dismissChild(currentView: self)
    }
}
