

import Foundation
import UIKit


class Sauces: UIViewController {
    
    var identifiers: ([String], [Int]) = ([], [])
    
    private var saucesTuple: ([UIButton], [UIStepper], [UILabel]) = ([], [], [])
    private var saucesTupleConstraints: ([NSLayoutConstraint], [NSLayoutConstraint], [NSLayoutConstraint]) = ([], [], [])
    
    
    //MARK: - instance methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clear
        initButtons()
        resetConstrains()
        animateSauces()
    }
    
    private func initButtons() {
        saucesTuple = Menus.generateIngredientsTuple(view, buttonType: Buttons.saucesButton, names: Others.sortAlph(Strings.saucesString), backColor: .green, constant: -30)
        saucesTupleConstraints = Constraints.generateConstraintsTopTuple(view, button: saucesTuple.0, labels: saucesTuple.2, steppers: saucesTuple.1, constant: 0)
    }
    
    private func resetConstrains() {
        Constraints.resetConstraintsTuple(saucesTupleConstraints.0, constraint1: saucesTupleConstraints.1, constraint2: saucesTupleConstraints.2)
    }
    
    //MARK: - menu methods
    
    private func animateSauces() {
        Constraints.setConstraintsTuple(saucesTupleConstraints.0, constraint2: saucesTupleConstraints.1, constraint3: saucesTupleConstraints.2, x: -40, y: 45)
        
        for i in 0..<saucesTuple.1.count {
            saucesTuple.1[i].addTarget(self, action: #selector(stepperValueChanged), for: .touchUpInside)
        }
        
        UIView .animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            Menus.setAlphaTuple(self.saucesTuple.0, labels: self.saucesTuple.2, steppers: self.saucesTuple.1, alpha: 1)
        }
    }
    
    //MARK: - actions methods
    
    @objc private func stepperValueChanged(sender: UIStepper) {
        if identifiers.0.contains(sender.accessibilityIdentifier!) {
            for i in 0..<identifiers.0.count {
                if identifiers.0[i] == sender.accessibilityIdentifier {
                    if (identifiers.1[i] == 5 && sender.value > 0)       {
                        Others.errorAlert(self, title: "Maxed out", message: "maximum quantity achieved")
                                                                          break}
                    if (identifiers.1[i] == 0 && sender.value <= 0)      {break}
                    if sender.value > 0                                  {identifiers.1[i] += 1}
                    else                                                 {identifiers.1[i] -= 1}
                }
            }
        } else {
            identifiers.0.insert(sender.accessibilityIdentifier!, at: 0)
            identifiers.1.insert(1, at: 0)
        }
        identifiers = Others.removeZero(identifiers)
        if let parentVC = self.parent as? CreatePizza {
            parentVC.ingredients = identifiers
            parentVC.calculatePrice()
            parentVC.tableView.reloadData()
        }
        sender.value = 0
    }
    
    @IBAction func back(_ sender: UIButton) {
        if let parentVC = self.parent as? CreatePizza {
            parentVC.hideView(1)
            parentVC.ingredients = identifiers
        }
        Others.dismissChild(currentView: self)
    }
}
