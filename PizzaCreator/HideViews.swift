


import Foundation
import UIKit

class HideViews: NSObject {
    
    class func transparentNav(_ navigationBar: UINavigationBar) {
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
    }
    
    class func alphaMenu(_ x: CGFloat, doughIcon: UIButton, saucesIcon: UIButton, toppingIcon: UIButton, doughLabel: UILabel, saucesLabel: UILabel, toppingLabel: UILabel, navigationBar: UINavigationBar) {
        doughIcon.alpha = x
        saucesIcon.alpha = x
        toppingIcon.alpha = x
        doughLabel.alpha = x
        saucesLabel.alpha = x
        toppingLabel.alpha = x
        navigationBar.alpha = x
    }
}
